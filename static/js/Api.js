class Api {
  constructor(http) {
    this._http = http
    this._methodGet = "GET"
    this._methodPost = "POST"
  }

  showList() {
    return this._http({
      method: this._methodGet,
      url: "/getProductList",
    })
  }

  addProduct(data) {
    return this._http({
      method: this._methodPost,
      url: "/addProduct",
      data: {
        info: data,
      },
    })
  }

  updateProduct(data) {
    return this._http({
      method: this._methodPost,
      url: "/updateProduct",
      data: {
        info: data,
      },
    })
  }

  deleteProduct(id) {
    return this._http({
      method: this._methodPost,
      url: "/deleteProduct",
      data: {
        id: id,
      },
    })
  }
}