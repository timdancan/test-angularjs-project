const app = angular.module("myApp", []);

$('.dropdown-toggle').dropdown()

app.controller("HomeCtrl", function ($http) {
  const vm = this;
  vm.popupTypeAdd = document.querySelector(".modal_add-popup");
  vm.popupTypeConfirm = document.querySelector(".modal_confirm-popup");
  vm.popupTypeEdit = document.querySelector(".modal_edit-popup");
  vm.info = {};
  vm.showAdd = true;
  vm.productName = "";
  vm.price = "";
  vm.description = "";
  vm.count = "";
  vm.currentTurget = null;
  vm.api = new Api($http);
  vm.sortOption = {
    sortFieldName: null,
    isAsc: false,
  }
  vm.fieldNames = ['product', 'price', 'description', 'count']

  vm.showlist = function () {
    vm.api.showList()
      .then(
      function (response) {
        vm.products = response.data;
      },
      function (error) {
        console.log(error);
      }
    );
  };
  vm.showlist();

  vm.openPopup = function (popup) {
    popup.classList.add("modal_visiable");
  };

  vm.closePopup = function (popup) {
    popup.classList.remove("modal_visiable");
  };

  vm.showAddPopUp = function () {
    vm.openPopup(vm.popupTypeAdd);
  };

  vm.closeAddPopup = function () {
    vm.closePopup(vm.popupTypeAdd);
  };

  vm.showConfirmPopUp = function (id) {
    vm.currentTurget = vm.products.find((item) => item.id === id);
    vm.productName = vm.currentTurget.product;
    console.log(this.productName);
    vm.openPopup(vm.popupTypeConfirm);
  };

  vm.closeConfirmPopUp = function () {
    vm.closePopup(vm.popupTypeConfirm);
  };

  vm.showEditPopUp = function (id) {
    vm.currentTurget = vm.products.find((item) => item.id === id);
    vm.productName = vm.currentTurget.product;
    vm.price = vm.currentTurget.price;
    vm.description = vm.currentTurget.description;
    vm.count = vm.currentTurget.count;
    vm.openPopup(vm.popupTypeEdit);
  };

  vm.closeEditPopUp = function () {
    vm.closePopup(vm.popupTypeEdit);
  };

  vm.addProduct = function () {
    const info = {
      product: vm.productName,
      price: vm.price,
      description: vm.description,
      count: vm.count,
    };
    vm.api.addProduct(info)
      .then(function () {
        vm.closeAddPopup();
      })
      .then(function () {
        vm.showlist();
      })
      .catch((e) => console.log(e));
  };

  vm.updateProduct = function () {
    const info = {
      id: this.currentTurget.id,
      product: this.productName,
      price: this.price,
      description: this.description,
      count: this.count,
    };
    vm.api.updateProduct(info)
      .then(function () {
        vm.closeEditPopUp();
      })
      .then(function () {
        vm.showlist();
      })
      .catch((e) => console.log(e));
  };

  vm.deleteProduct = function () {
    vm.api.deleteProduct(vm.currentTurget.id)
      .then(function () {
        vm.closeConfirmPopUp();
      })
      .then(function () {
        vm.showlist();
      })
      .catch((e) => console.log(e));
  };

  vm.sortByField = function (field) {
    console.log(field);
    console.log(vm.products);
    if (field === 'count' || field === 'price') {
      if(vm.sortOption.isAsc) {
        vm.products.sort((a, b) => a[field] - b[field])
      } else {
        vm.products.sort((a, b) => b[field] - a[field])
      }
    } else {
      vm.products.sort()
    }
    
  };

  vm.sort = function (field) {
    vm.sortOption.sortFieldName = field
    vm.sortOption.isAsc = !vm.sortOption.isAsc
    vm.sortByField(field)
  }
});
