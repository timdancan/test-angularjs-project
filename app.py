from pymongo import MongoClient
from bson.objectid import ObjectId
from flask import Flask,render_template,jsonify,json,request

application = Flask(__name__)

client = MongoClient('localhost:27017')
db = client.ProductData

@application.route("/addProduct",methods=['POST'])
def addProduct():
    try:

        json_data = request.json['info']
        productName = json_data['product']
        productPrice = json_data['price']
        description = json_data['description']
        countNumber = json_data['count']
    

        db.Products.insert_one({
            'product':productName,'price':productPrice,'description':description,'count':countNumber
            })
        return jsonify(status='OK',message='inserted successfully')

    except Exception:
        return jsonify(status='ERROR',message=str())

@application.route('/')
def showProductList():
    return render_template('list.html')

@application.route('/getProduct',methods=['GET'])
def getProduct():
    try:
        productId = request.json['id']
        product = db.Products.find_one({'_id':ObjectId(productId)})
        productDetail = {
                'product':product['product'],
                'price':product['price'],
                'description':product['description'],
                'count':product['count'],
                'id':str(product['_id'])
                }
        return json.dumps(productDetail)
    except Exception:
        return str()

@application.route('/updateProduct',methods=['POST'])
def updateProduct():
    try:
        productInfo = request.json['info']
        productId = productInfo['id']
        product = productInfo['product']
        price = productInfo['price']
        description = productInfo['description']
        count = productInfo['count']

        db.Products.update_one({'_id':ObjectId(productId)},{'$set':{'product':product,'price':price,'description':description,'count':count}})
        return jsonify(status='OK',message='updated successfully')
    except Exception:
        return jsonify(status='ERROR',message=str())

@application.route("/getProductList",methods=['GET'])
def getProductList():
    try:
        products = db.Products.find()
        
        productList = []
        for product in products:
            print(product)
            productItem = {
                    'product':product['product'],
                    'price':product['price'],
                    'description':product['description'],
                    'count':product['count'],
                    'id': str(product['_id'])
                    }
            productList.append(productItem)
    except Exception:
        return str()
    return json.dumps(productList)

@application.route("/deleteProduct",methods=['POST'])
def deleteProduct():
    try:
        productId = request.json['id']
        db.Products.remove({'_id':ObjectId(productId)})
        return jsonify(status='OK',message='deletion successful')
    except Exception:
        return jsonify(status='ERROR',message=str())

if __name__ == "__main__":
    application.run(host='0.0.0.0')

