# PythonFlaskRemoteApp
Code for blog post: http://codehandbook.org/creating-a-web-app-using-angularjs-python-mongodb/

## Dependencies 

### Install MongoDB
https://docs.mongodb.com/manual0/installation/

### Install PIP
https://pip.pypa.io/en/stable/installing/

### Install Flask 
https://flask.palletsprojects.com/en/1.1.x/installation/

### Install Flask Dependencies
```bash
# Install Flask pymongo dependency (NOTE: Mac probably requires sudo)
 pip3 install pymongo
 
 #Install Flask fabric dependency
pip3 install fabric
```

## Usage
```bash
python app.py
```

View app at http://localhost:5000/

# pip3 install flask
# pip3 install fabric3
